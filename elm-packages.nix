{

      "elm-community/dict-extra" = {
        sha256 = "05ll04wf03m8ic109dz2dbq6pah23m70c4wwyr35026dhmws35n0";
        version = "2.4.0";
      };

      "elm-community/list-extra" = {
        sha256 = "02grd0p5hc2gvdy4n723d1s28pm1grn95jrzic6jcgb26qh16vcc";
        version = "8.7.0";
      };

      "elm-community/maybe-extra" = {
        sha256 = "0qslmgswa625d218djd3p62pnqcrz38f5p558mbjl6kc1ss0kzv3";
        version = "5.2.0";
      };

      "elm-community/result-extra" = {
        sha256 = "0bwiqjq4cgffbk8a6nqk1k4yhv1hwg96m2fhn5zbniwsm13lrm5m";
        version = "2.4.0";
      };

      "elm/browser" = {
        sha256 = "0nagb9ajacxbbg985r4k9h0jadqpp0gp84nm94kcgbr5sf8i9x13";
        version = "1.0.2";
      };

      "elm/core" = {
        sha256 = "19w0iisdd66ywjayyga4kv2p1v9rxzqjaxhckp8ni6n8i0fb2dvf";
        version = "1.0.5";
      };

      "elm/html" = {
        sha256 = "1n3gpzmpqqdsldys4ipgyl1zacn0kbpc3g4v3hdpiyfjlgh8bf3k";
        version = "1.0.0";
      };

      "elm/http" = {
        sha256 = "008bs76mnp48b4dw8qwjj4fyvzbxvlrl4xpa2qh1gg2kfwyw56v1";
        version = "2.0.0";
      };

      "elm/json" = {
        sha256 = "0kjwrz195z84kwywaxhhlnpl3p251qlbm5iz6byd6jky2crmyqyh";
        version = "1.1.3";
      };

      "elm/parser" = {
        sha256 = "0a3cxrvbm7mwg9ykynhp7vjid58zsw03r63qxipxp3z09qks7512";
        version = "1.1.0";
      };

      "elm/random" = {
        sha256 = "138n2455wdjwa657w6sjq18wx2r0k60ibpc4frhbqr50sncxrfdl";
        version = "1.0.0";
      };

      "elm/regex" = {
        sha256 = "0lijsp50w7n1n57mjg6clpn9phly8vvs07h0qh2rqcs0f1jqvsa2";
        version = "1.0.0";
      };

      "elm/svg" = {
        sha256 = "1cwcj73p61q45wqwgqvrvz3aypjyy3fw732xyxdyj6s256hwkn0k";
        version = "1.0.1";
      };

      "elm/time" = {
        sha256 = "0vch7i86vn0x8b850w1p69vplll1bnbkp8s383z7pinyg94cm2z1";
        version = "1.0.0";
      };

      "krisajenkins/remotedata" = {
        sha256 = "0m5bk0qhsjv14vajqrkph386696pnhj5rn51kgma8lwyvvx9ihw1";
        version = "6.0.1";
      };

      "mpizenberg/elm-pointer-events" = {
        sha256 = "16s14sh01g6ssabwkf2k1xdxnahnkn0s7603cg87wd0h4myg15da";
        version = "4.0.2";
      };

      "ohanhi/keyboard" = {
        sha256 = "10sbq8v2kydnc3lkydl367g36q2b0xizxl031xyakrgl4zlh07ic";
        version = "2.0.1";
      };

      "tad-lispy/springs" = {
        sha256 = "1zrvgs0mrwbc6rp4asrb0rkrddjwv9ck4g5zz706a36v93y824dp";
        version = "1.0.5";
      };

      "elm/bytes" = {
        sha256 = "02ywbf52akvxclpxwj9n04jydajcbsbcbsnjs53yjc5lwck3abwj";
        version = "1.0.8";
      };

      "elm/file" = {
        sha256 = "1rljcb41dl97myidyjih2yliyzddkr2m7n74x7gg46rcw4jl0ny8";
        version = "1.0.5";
      };

      "elm/url" = {
        sha256 = "0av8x5syid40sgpl5vd7pry2rq0q4pga28b4yykn9gd9v12rs3l4";
        version = "1.0.0";
      };

      "elm/virtual-dom" = {
        sha256 = "0q1v5gi4g336bzz1lgwpn5b1639lrn63d8y6k6pimcyismp2i1yg";
        version = "1.0.2";
      };

      "elm-explorations/test" = {
        sha256 = "16lpk71aiw6cz4g804sra0gzssqyp6w1s4c2zdnyywmfwwnxiw4s";
        version = "2.1.0";
      };
}
