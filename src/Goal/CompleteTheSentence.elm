module Goal.CompleteTheSentence exposing
    ( Charset
    , Model
    , Sentence
    , collect
    , init
    , isComplete
    , isCorrect
    , missing
    , needsNext
    )

import Area exposing (Area)
import Position exposing (Position)


type alias Model =
    { sentence : Sentence
    , collected : String
    , area : Area
    }


type alias Sentence =
    { charset : Charset
    , intro : String
    , password : String
    , outro : String
    }


type alias Charset =
    List Char


init : Position -> Sentence -> Model
init position sentence =
    { area = Area.centerAround position Area.default
    , collected = ""
    , sentence = sentence
    }


isComplete : Model -> Bool
isComplete model =
    String.toUpper model.collected == String.toUpper model.sentence.password


missing : Model -> List Char
missing model =
    model.sentence.password
        |> String.toUpper
        |> String.toList
        |> List.drop (String.length model.collected)


collect : Char -> Model -> Model
collect letter model =
    if isCorrect letter model then
        append letter model

    else
        model


{-| Blindly appends the given letter to the collected list

Does not check if it's correct, so probably only good for internal use.

-}
append : Char -> Model -> Model
append letter model =
    { model
        | collected =
            model.collected
                |> String.reverse
                |> String.cons letter
                |> String.reverse
    }


isCorrect : Char -> Model -> Bool
isCorrect letter model =
    needsNext model == Just letter


needsNext : Model -> Maybe Char
needsNext model =
    model
        |> missing
        |> List.head
