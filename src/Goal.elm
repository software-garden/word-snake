module Goal exposing
    ( Charset
    , Collectible
    , Goal(..)
    , Sentence
    , collectLetter
    , collectSingle
    , coundown
    , isComplete
    , missingLetters
    , nextLetter
    , restart
    , sentenceCompleted
    , serialize
    )

import Direction exposing (Direction(..))
import Entities exposing (Entity)
import Goal.CompleteTheSentence as CompleteTheSentence
import Html exposing (Html)
import Position exposing (Position)


type Goal
    = CompleteTheSentence CompleteTheSentence.Model
    | ReadInstructions (List (Html Never))
    | ChangeDirections (List Direction)
    | CollectSingle Charset (List Collectible)


type alias Collectible =
    { entity : Entity
    , label : String
    }


coundown : Goal -> Int
coundown goal =
    -- TODO: Let the countdown be part of relevant goal models
    case goal of
        CompleteTheSentence _ ->
            5

        ReadInstructions _ ->
            0

        ChangeDirections _ ->
            0

        CollectSingle _ _ ->
            5


isComplete : Goal -> Bool
isComplete goal =
    case goal of
        CompleteTheSentence model ->
            CompleteTheSentence.isComplete model

        ReadInstructions _ ->
            True

        ChangeDirections directions ->
            List.isEmpty directions

        CollectSingle _ collectibles ->
            List.isEmpty collectibles


serialize : Goal -> String
serialize goal =
    case goal of
        CompleteTheSentence { sentence } ->
            [ "< "
            , sentence.charset
                |> List.map String.fromChar
                |> String.join " "
            , " > "
            , sentence.intro
            , "{ "
            , sentence.password
            , " }"
            , sentence.outro
            ]
                |> String.join ""

        ReadInstructions _ ->
            ""

        ChangeDirections _ ->
            ""

        CollectSingle _ _ ->
            ""



-- COMPLETE THE SENTENCE


type alias Sentence =
    { charset : Charset
    , intro : String
    , password : String
    , outro : String
    }


type alias Charset =
    List Char


sentenceCompleted : Sentence -> String -> Bool
sentenceCompleted sentence collected =
    collected
        |> missingLetters sentence
        |> List.isEmpty


missingLetters : Sentence -> String -> List Char
missingLetters sentence collected =
    sentence.password
        |> String.toUpper
        |> String.toList
        |> List.drop (String.length collected)


collectLetter : Sentence -> Char -> String -> String
collectLetter sentence letter collected =
    if isNextLetter letter sentence collected then
        collected
            |> String.reverse
            |> String.cons letter
            |> String.reverse

    else
        collected


isNextLetter : Char -> Sentence -> String -> Bool
isNextLetter letter sentence collected =
    nextLetter sentence collected == Just letter


nextLetter : Sentence -> String -> Maybe Char
nextLetter sentence collected =
    collected
        |> missingLetters sentence
        |> List.head



-- COLLECT SINGLE


collectSingle : Entity -> List Collectible -> List Collectible
collectSingle entity collectibles =
    case collectibles of
        collectible :: rest ->
            if entity == collectible.entity then
                rest

            else
                collectibles

        [] ->
            collectibles


restart : Position -> Goal -> Goal
restart position goal =
    case goal of
        CompleteTheSentence completion ->
            completion.sentence
                |> CompleteTheSentence.init position
                |> CompleteTheSentence

        ReadInstructions _ ->
            goal

        ChangeDirections _ ->
            goal

        CollectSingle _ _ ->
            goal
