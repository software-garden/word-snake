module Pan exposing
    ( Pan
    , animate
    , new
    , setTarget
    , value
    )

import Coordinates exposing (Coordinates)
import Spring exposing (Spring)


{-| A pan is coordinates on springs
-}
type alias Pan =
    { x : Spring
    , y : Spring
    }


new : Float -> Float -> Pan
new strength dampness =
    Pan
        (Spring.create { strength = strength, dampness = dampness })
        (Spring.create { strength = strength, dampness = dampness })


setTarget : Coordinates -> Pan -> Pan
setTarget target pan =
    Pan
        (Spring.setTarget target.x pan.x)
        (Spring.setTarget target.y pan.y)


value : Pan -> Coordinates
value pan =
    { x = Spring.value pan.x
    , y = Spring.value pan.y
    }


animate : Float -> Pan -> Pan
animate delta pan =
    { x = Spring.animate delta pan.x
    , y = Spring.animate delta pan.y
    }
