port module Ports exposing (fullscreen, goalTracking, storeOutcome)

import Game
import Goal exposing (Goal)


port fullscreen : () -> Cmd msg


port goalTracking : ( String, Int ) -> Cmd msg


port outcomes : ( String, Bool ) -> Cmd msg


storeOutcome : Goal -> Game.Outcome -> Cmd msg
storeOutcome level outcome =
    case outcome of
        Game.InProgress ->
            Cmd.none

        Game.Won ->
            outcomes
                ( Goal.serialize level
                , True
                )

        Game.Lost ->
            outcomes
                ( Goal.serialize level
                , False
                )
