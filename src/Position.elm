module Position exposing
    ( Position
    , line
    , neighborhood
    , sector
    , sectorLength
    , shift
    , trace
    )

import Direction exposing (Direction(..))
import List.Extra as List


type alias Position =
    -- TODO: Convert to record with x and y fields for more expressive code?
    ( Int
    , Int
    )


shift : Direction -> Position -> Position
shift direction ( x, y ) =
    case direction of
        North ->
            ( x, y - 1 )

        East ->
            ( x + 1, y )

        South ->
            ( x, y + 1 )

        West ->
            ( x - 1, y )


neighborhood : Position -> List Position
neighborhood ( x, y ) =
    [ ( x - 1, y - 1 )
    , ( x, y - 1 )
    , ( x + 1, y - 1 )
    , ( x - 1, y )
    , ( x, y )
    , ( x + 1, y )
    , ( x - 1, y + 1 )
    , ( x, y + 1 )
    , ( x + 1, y + 1 )
    ]


{-| Returns a list of consecutive positions in a given direction, starting from a given position

    import Direction exposing (Direction(..))

    line North 3 ( 4, 5 )
    --> [ ( 4, 5 ), ( 4, 4 ), ( 4, 3 ) ]

Given a length of 0 or negative, will produce an empty list:

    import Direction exposing (Direction(..))

    line South 0 (5, 3)
    --> []

A line with a length of 1 will contain only the original position:

    import Direction exposing (Direction(..))

    line Direction.South 1 (3, -12)
    --> [ (3, -12)]

-}
line : Direction -> Int -> Position -> List Position
line direction length from =
    if length < 1 then
        []

    else
        let
            shifted =
                shift direction from
        in
        from :: line direction (length - 1) shifted


{-| Given a list of directions returns a list of intermediate positions starting at ( 0, 0 )

    import Direction exposing (Direction(..))


    trace (2, -3) [ North, North, East, South ]
    --> [ (2, -3)
    --> , (2, -4)
    --> , (2, -5)
    --> , (3, -5)
    --> , (3, -4)
    --> ]

Useful for analyzing the snake body.

-}
trace : Position -> List Direction -> List Position
trace start directions =
    List.scanl shift start directions



-- SECTOR
-- TODO: Move to own module?


{-| Given a sector size and a position gives the position of the sector

    Sectors provide a logical division of the game space. They are useful for drawing background. Since the space is unbound (infinite), we only want to draw background where it's needed, i.e. where the player is looking (or was looking in the past).

    A sector is a square area that neighbors 8 other sectors. It's size is the number of positions around it's center in each direction.

    Sector ( 0, 0 ) is always centered at position ( 0, 0 ).  Given size of 4, it spans from and including ( -4, -4 ) to ( 4, 4 ). The length of it's edge is 9 (-4, -3, -2, -1, 0, 1, 2, 3, 4) and it contains 81 (9^2) individual positions. To the east lays sector ( 1, 0 ), with center at ( 9, 0 ) that spans 81 fields between (5, -4) and (13, 4). Any position within this range belongs to this sector.



        sector 4 ( 0, 0 )
        --> ( 0, 0 )


        sector 4 ( 10, -8 )
        --> ( 1, -1 )

        sector 8 ( -43, 22 )
        --> ( -3, 1 )

-}
sector : Int -> Position -> Position
sector size ( x, y ) =
    let
        length =
            sectorLength size

        coordinate position =
            round (toFloat position / toFloat length)
    in
    ( coordinate x
    , coordinate y
    )


sectorLength : Int -> Int
sectorLength size =
    size * 2 + 1
