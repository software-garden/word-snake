module Tutorial exposing (goals, intro)

import Direction exposing (Direction(..))
import Goal exposing (Goal)
import Goal.CompleteTheSentence as CompleteTheSentence
import Html
import Tad.Html as Html


intro : Goal
intro =
    Goal.ReadInstructions
        [ "Welcome to Word Snake!" |> Html.text |> Html.wrap Html.h1 []
        , "In this tutorial we will show you basic elements of the game." |> Html.text |> Html.wrap Html.p []
        , "Please note, the tutorial is still work in progress."
            |> Html.text
            |> Html.wrap Html.em []
            |> Html.wrap Html.p []
        ]


goals : List Goal
goals =
    [ Goal.ReadInstructions
        [ "Moving around" |> Html.text |> Html.wrap Html.h1 []
        , "Use W A S D or swipe 👆 on a touch-screen to move around." |> Html.text |> Html.wrap Html.p []
        , "Let's practice! In the next lesson you will control the snake. Follow the instructions on top of the screen." |> Html.text |> Html.wrap Html.p []
        ]
    , Goal.ChangeDirections [ North, West, East, South ]
    , Goal.ReadInstructions
        [ "Collecting stuff" |> Html.text |> Html.wrap Html.h1 []
        , "The main goal of the game is to collect the letters and complete sentences. Let's practice collecting on fruits for now." |> Html.text |> Html.wrap Html.p []
        , "Navigate the Snake to collect an apple 🍏, some grapes 🍇 and a delicious pear 🍐." |> Html.text |> Html.wrap Html.p []
        ]
    , Goal.CollectSingle
        []
        [ Goal.Collectible '🍏' "an apple"
        , Goal.Collectible '🍇' "grapes"
        , Goal.Collectible '🍐' "a pear"
        ]
    , Goal.ReadInstructions
        [ "Eating healthy" |> Html.text |> Html.wrap Html.h1 []
        , "Did you notice how the Snake grew as they were collecting fruits? That's right! Each time they collect something, their length increases by one. But not everything is the right thing to collect at a given time. Because the longer the snake gets, the more difficult it is to navigate, try to avoid collecting useless stuff. In addition to making the snake longer, collecting junk will also make them poop. Running into a poo is deadly!" |> Html.text |> Html.wrap Html.p []
        , "In the next exercise you will be tasked with collecting fruits again, but this time there will also be mushrooms. Do not collect them!" |> Html.text |> Html.wrap Html.p []
        ]
    , Goal.CollectSingle
        [ '🍄', '🍄', '🍄', '🍄', '🍄'  ]
        [ Goal.Collectible '🍏' "an apple"
        , Goal.Collectible '🍇' "grapes"
        , Goal.Collectible '🍐' "a pear"
        ]
    , Goal.ReadInstructions
        [ "Collecting letters" |> Html.text |> Html.wrap Html.h1 []
        , "Navigate the Snake to collect the letters and fill the missing word in a sentence. Once the password is collected you will be able to escape the fire trap." |> Html.text |> Html.wrap Html.p []
        ]
    , CompleteTheSentence.Sentence
        [ 'A', 'B', 'C' ]
        "Collect letters A B and C in order: "
        "ABC"
        ""
        -- For now area is centered at ( 0, 0 ), but will be reset when the game starts
        |> CompleteTheSentence.init (0, 0)
        |> Goal.CompleteTheSentence
    , Goal.ReadInstructions
        [ "Save the Snake!" |> Html.text |> Html.wrap Html.h1 []
        , "You have finished the tutorial. Well done! Now play." |> Html.text |> Html.wrap Html.p []
        , "The most important: avoid fire and poop." |> Html.text |> Html.wrap Html.p []
        ]
    ]
