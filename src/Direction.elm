module Direction exposing
    ( Direction(..)
    , arrow
    , opposite
    , toString
    )


type Direction
    = North
    | East
    | South
    | West


toString : Direction -> String
toString direction =
    case direction of
        North ->
            "north"

        East ->
            "east"

        South ->
            "south"

        West ->
            "west"


arrow : Direction -> String
arrow direction =
    case direction of
        North ->
            "👆"

        East ->
            "👉"

        South ->
            "👇"

        West ->
            "👈"


opposite : Direction -> Direction
opposite direction =
    case direction of
        North ->
            South

        East ->
            West

        South ->
            North

        West ->
            East
