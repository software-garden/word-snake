module Coordinates exposing
    ( Coordinates
    , add
    , fromPosition
    , midpoint
    , multiply
    , shift
    , subtract
    , toString
    , toTransformation
    )

import Direction exposing (Direction(..))
import Position exposing (Position)
import Transformations exposing (Transformation)


type alias Coordinates =
    { x : Float
    , y : Float
    }


{-| Given a position and scale, returns a coordinate

    fromPosition 200 (3, -5)
    --> { x = 600
    --> , y = -1000
    --> }

-}
fromPosition : Float -> Position -> Coordinates
fromPosition scale ( x, y ) =
    { x = x |> toFloat |> (*) scale
    , y = y |> toFloat |> (*) scale
    }


{-| Given a displacement and origin, returns displaced coordinates

Basically a 2d vector sum.

    add
        { x = 100 , y = -300 }
        { x = -400, y = 500 }
    --> { x = -300, y = 200 }

-}
add : Coordinates -> Coordinates -> Coordinates
add displacement origin =
    { x = origin.x + displacement.x
    , y = origin.y + displacement.y
    }


subtract : Coordinates -> Coordinates -> Coordinates
subtract displacement origin =
    displacement
        |> multiply -1
        |> add origin


{-| If you think about the coordinates as a 2d vector, you can multiply it

This is useful for inverting the vector or finding a midpoint.

    Coordinates 12.5 -20
        |> multiply 0.5
    --> Coordinates 6.25 -10

-}
multiply : Float -> Coordinates -> Coordinates
multiply factor { x, y } =
    Coordinates
        (x * factor)
        (y * factor)


{-| Find a point in the middle between two coordinates

    midpoint
        (Coordinates 50  20)
        (Coordinates 20 -20)
    --> Coordinates  35  0

-}
midpoint : Coordinates -> Coordinates -> Coordinates
midpoint a b =
    a
        |> subtract b
        |> multiply 0.5
        |> add b


shift : Float -> Direction -> Coordinates -> Coordinates
shift distance direction coordinates =
    case direction of
        North ->
            add coordinates
                { x = 0
                , y = -distance
                }

        East ->
            add coordinates
                { x = distance
                , y = 0
                }

        South ->
            add coordinates
                { x = 0
                , y = distance
                }

        West ->
            add coordinates
                { x = -distance
                , y = 0
                }


toString : Coordinates -> String
toString { x, y } =
    [ x, y ]
        |> List.map String.fromFloat
        |> String.join ", "


toTransformation : String -> Coordinates -> Transformation
toTransformation unit { x, y } =
    Transformations.Translate unit x y
