module Entities exposing
    ( Entities
    , Entity
    , State(..)
    , collect
    , decay
    , get
    , isPresent
    , neighbors
    , spawn
    , stateAt
    , step
    )

{-| The Entities data structure represents environment that the snake can
interact with, like letters to collect, poops and flames that can kill it, etc.
-}

import Dict exposing (Dict)
import Dict.Extra as Dict
import Maybe.Extra as Maybe
import Position exposing (Position)


type alias Entities =
    Dict Position State


type State
    = Empty
    | Appearing Entity
    | Present Entity
    | Disappearing Entity
    | Replacing Entity Entity
    | Collected Entity


type alias Entity =
    Char


get : Position -> Entities -> List Entity
get position entities =
    case stateAt position entities of
        Empty ->
            []

        Appearing entity ->
            [ entity ]

        Present entity ->
            [ entity ]

        Disappearing entity ->
            [ entity ]

        Replacing old new ->
            [ old, new ]

        Collected entity ->
            [ entity ]


stateAt : Position -> Entities -> State
stateAt position entities =
    entities
        |> Dict.get position
        |> Maybe.withDefault Empty



-- UPDATING


step : Entities -> Entities
step entities =
    Dict.filterMap stepEntity entities


stepEntity : Position -> State -> Maybe State
stepEntity _ state =
    case state of
        Empty ->
            Nothing

        Appearing entity ->
            Present entity |> Just

        Present _ ->
            state |> Just

        Disappearing _ ->
            Empty |> Just

        Replacing old new ->
            Present new |> Just

        Collected _ ->
            Nothing


spawn : Position -> Entity -> Entities -> Entities
spawn position entity entities =
    case stateAt position entities of
        Empty ->
            entities
                |> Dict.insert position (Appearing entity)

        Appearing new ->
            -- Swap previous new with the new new
            entities
                |> Dict.insert position (Appearing entity)

        Present old ->
            entities
                |> Dict.insert position (Replacing old entity)

        Disappearing old ->
            entities
                |> Dict.insert position (Replacing old entity)

        Replacing old new ->
            -- Swap previous new with the new new
            entities
                |> Dict.insert position (Replacing old entity)

        Collected _ ->
            -- If something was just collected, ignore the insertion.
            entities


collect : Position -> Entity -> Entities -> Entities
collect position entity entities =
    -- NOTE: We trust that the entity was there to collect.
    Dict.insert position (Collected entity) entities


decay : Position -> Entities -> Entities
decay position entities =
    case stateAt position entities of
        Empty ->
            entities

        Appearing entity ->
            entities
                |> Dict.insert position (Disappearing entity)

        Present entity ->
            entities
                |> Dict.insert position (Disappearing entity)

        Disappearing _ ->
            entities

        Replacing old new ->
            entities
                |> Dict.insert position (Disappearing new)

        Collected _ ->
            entities


isPresent : Entity -> Entities -> Bool
isPresent entity entities =
    Dict.any (contains entity) entities


contains : Entity -> Position -> State -> Bool
contains searching position state =
    case state of
        Empty ->
            False

        Appearing entity ->
            searching == entity

        Present entity ->
            searching == entity

        Disappearing entity ->
            searching == entity

        Replacing old new ->
            searching == old && searching == new

        Collected entity ->
            searching == entity


neighbors : Position -> Entities -> List Entity
neighbors position entities =
    position
        |> Position.neighborhood
        |> List.concatMap (\neighbor -> get neighbor entities)
