module Transformations exposing
    ( Transformation(..)
    , toString
    )


type Transformation
    = Identity
    | Scale Float Float
    | Translate String Float Float
    | Rotate Float


toString : Transformation -> String
toString transformation =
    case transformation of
        Identity ->
            ""

        Scale x y ->
            "scale("
                ++ String.fromFloat x
                ++ ", "
                ++ String.fromFloat y
                ++ ")"

        Translate unit x y ->
            "translate("
                ++ String.fromFloat x
                ++ unit
                ++ ","
                ++ String.fromFloat y
                ++ unit
                ++ ")"

        Rotate angle ->
            "rotate("
                ++ String.fromFloat angle
                ++ ")"
