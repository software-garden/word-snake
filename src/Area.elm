module Area exposing
    ( Area
    , centerAround
    , centerX
    , centerY
    , default
    , edge
    , isInside
    , isOutside
    , maxX
    , maxY
    , minX
    , minY
    , randomPosition
    )

import Position exposing (Position)
import Random


type alias Area =
    { width : Int
    , height : Int
    , center : Position
    }


default : Area
default =
    Area 16 16 ( 0, 0 )


{-| Edge is a collection of positions forming a rectangle around the center

    import Set

    Area 5 5 ( 3, 2 )
        |> edge
        |> Set.fromList

    --> [ (1, 0), (2, 0), (3, 0), (4, 0), (5, 0)
    --> , (1, 1)                        , (5, 1)
    --> , (1, 2)        {- 3, 2 -}      , (5, 2)
    --> , (1, 3)                        , (5, 3)
    --> , (1, 4), (2, 4), (3, 4), (4, 4), (5, 4)
    --> ]
    -->     |> Set.fromList

-}
edge : Area -> List Position
edge area =
    let
        northEdge : List Position
        northEdge =
            area
                |> maxX
                |> List.range (minX area)
                |> List.map (\x -> ( x, minY area ))

        eastEdge : List Position
        eastEdge =
            area
                |> maxY
                |> List.range (minY area)
                |> List.map (\y -> ( maxX area, y ))

        southEdge : List Position
        southEdge =
            area
                |> maxX
                |> List.range (minX area)
                |> List.map (\x -> ( x, maxY area ))

        westEdge : List Position
        westEdge =
            area
                |> maxY
                |> List.range (minY area)
                |> List.map (\y -> ( minX area, y ))
    in
    List.concat
        [ northEdge
        , eastEdge
        , southEdge
        , westEdge
        ]


maxX : Area -> Int
maxX area =
    centerX area + (area.width // 2)


minX : Area -> Int
minX area =
    centerX area - (area.width // 2)


maxY : Area -> Int
maxY area =
    centerY area + (area.height // 2)


minY : Area -> Int
minY area =
    centerY area - (area.height // 2)


centerX : Area -> Int
centerX { center } =
    Tuple.first center


centerY : Area -> Int
centerY { center } =
    Tuple.second center


isInside : Area -> Position -> Bool
isInside area position =
    not <| isOutside area position


isOutside : Area -> Position -> Bool
isOutside area ( x, y ) =
    (x < minX area) || (x > maxX area) || (y < minY area) || (y > maxY area)


randomPosition : Area -> Random.Generator Position
randomPosition area =
    Random.map2 Tuple.pair
        (Random.int (minX area) (maxX area))
        (Random.int (minY area) (maxY area))




centerAround : Position -> Area -> Area
centerAround center area =
    { area | center = center }
