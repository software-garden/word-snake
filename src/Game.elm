port module Game exposing
    ( Model
    , Msg(..)
    , Outcome(..)
    , continue
    , hasWon
    , init
    , outcome
    , subscriptions
    , update
    , view
    , viewSentence
    , viewWorld
    )

import Area exposing (Area)
import Browser.Events
import Coordinates exposing (Coordinates)
import Dict
import Direction exposing (Direction(..))
import Entities exposing (Entities, Entity)
import Goal exposing (Goal(..))
import Goal.CompleteTheSentence as CompleteTheSentence
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Html.Events.Extra.Touch as Touch
import Json.Decode as Decode
import Keyboard
import List.Extra as List
import Maybe.Extra as Maybe
import Pan exposing (Pan)
import Position exposing (Position)
import Random
import Set exposing (Set)
import Snake exposing (Snake)
import String.Extra as String
import Svg exposing (Svg)
import Svg.Attributes
import Svg.Keyed
import Tad.Basics exposing (..)
import Tad.Html as Html
import Tad.String as String
import Time
import Transformations


port keydown : (Decode.Value -> msg) -> Sub msg


port preventDefault : Decode.Value -> Cmd msg


{-| Pace is the duration of a step in milliseconds
-}
pace : Float
pace =
    250


type alias Model =
    { viewbox : Int
    , tiles : Set Position
    , pan : Pan
    , entities : Entities
    , snake : Snake
    , goal : Goal
    , randomness : Random.Seed
    , swipe : Maybe Swipe
    , countdown : Int
    , progress : Float -- TODO: Progress is an amount of time (in ms) since last step. Find a better name.
    , annotations : List Annotation
    }


type alias Swipe =
    { identifier : Int
    , from : ( Float, Float )
    , to : ( Float, Float )
    }


type alias Annotation =
    { text : String
    , size : Float
    , coordinates : Coordinates
    }



-- INIT


type alias Flags =
    { goal : Goal
    , randomness : Random.Seed
    }


init : Flags -> Model
init flags =
    { tiles =
        ( 0, 0 )
            |> Position.neighborhood
            |> Set.fromList
    , pan =
        Pan.new 20 3
    , entities = Dict.empty
    , snake = Snake.new ( 0, 0 ) East 5
    , goal = flags.goal
    , randomness = flags.randomness
    , swipe = Nothing
    , countdown = Goal.coundown flags.goal
    , progress = 0
    , annotations = []
    , viewbox = 20
    }


continue : Goal -> Model -> Model
continue goal ({ snake, entities } as game) =
    { game
        | goal = Goal.restart snake.head goal
        , snake =
            if Snake.isAlive snake then
                snake

            else
                { snake
                    | death = Nothing
                    , body = []
                    , length = 5
                }
        , entities = clearPath snake.head snake.direction 5 entities
        , countdown = Goal.coundown goal
    }
        |> insertAnnotations game


clearPath : Position -> Direction -> Int -> Entities -> Entities
clearPath from direction length entities =
    from
        |> Position.line direction length
        |> List.foldl clearPosition entities


clearPosition : Position -> Entities -> Entities
clearPosition position =
    Dict.remove position



-- VIEW


{-| In view scale everything by an arbitrary factor.

In the model everything is positioned on a 1 x 1 grid, but Chromium and Android
Web View have problems with this setup. So in the view all values (positions,
sizes, font sizes, etc.) are scaled up by this factor. This is just a workaround
and has nothing to do with the game logic.

-}
scale : number
scale =
    1000


backgroundTileSize : Int
backgroundTileSize =
    16


view : Model -> Html Msg
view model =
    let
        content : List (Html Msg)
        content =
            case model.goal of
                ReadInstructions instructions ->
                    [ viewInstructions instructions ]

                CompleteTheSentence completion ->
                    [ viewSentence completion
                    , viewWorld model
                    , viewCurtain model.countdown model
                    ]

                ChangeDirections directions ->
                    [ viewDirections directions
                    , viewWorld model
                    , viewCurtain model.countdown model
                    ]

                CollectSingle _ collectibles ->
                    [ viewCollectibles collectibles
                    , viewWorld model
                    , viewCurtain model.countdown model
                    ]
    in
    content
        |> Html.div
            [ Html.Attributes.style "width" "100%"
            , Html.Attributes.style "height" "100%"
            , Html.Attributes.style "display" "flex"
            , Html.Attributes.style "justify-content" "center"
            , Html.Attributes.style "align-items" "center"
            , Html.Attributes.style "flex-direction" "column"
            ]


viewCollectibles : List Goal.Collectible -> Html Msg
viewCollectibles collectibles =
    case collectibles of
        current :: following ->
            viewHeader
                [ "Get " |> Html.text
                , current.label |> Html.text
                , " " |> Html.text
                , current.entity |> String.fromChar |> Html.text
                ]

        [] ->
            viewHeader
                [ "Well done!" |> Html.text ]


viewDirections : List Direction -> Html Msg
viewDirections directions =
    case directions of
        current :: following ->
            viewHeader
                [ "Turn " |> Html.text
                , current |> Direction.toString |> Html.text
                , " " |> Html.text
                , current |> Direction.arrow |> Html.text
                ]

        [] ->
            viewHeader
                [ "Well done!" |> Html.text ]


viewCurtain : Int -> Model -> Html Msg
viewCurtain countdown game =
    let
        overlay elements =
            elements
                |> Html.div
                    [ Html.Attributes.style "position" "absolute"
                    , Html.Attributes.style "top" "0"
                    , Html.Attributes.style "bottom" "0"
                    , Html.Attributes.style "left" "0"
                    , Html.Attributes.style "right" "0"
                    , Html.Attributes.style "display" "flex"
                    , Html.Attributes.style "justify-content" "center"
                    , Html.Attributes.style "background" "hsla(0,0%,0%,20%)"
                    , Html.Attributes.style "backdrop-filter" "blur(3px) saturate(50%)"
                    , Html.Attributes.style "animation-name" "overlay"
                    , Html.Attributes.style "animation-duration" "1s"
                    , Html.Attributes.style "animation-delay" "1s"
                    , Html.Attributes.style "animation-iteration-count" "1"
                    , Html.Attributes.style "animation-fill-mode" "both"
                    ]
    in
    case outcome game of
        InProgress ->
            if countdown == 0 then
                Html.div [] []

            else
                countdown
                    |> String.fromInt
                    |> Html.text
                    |> List.singleton
                    |> Html.div
                        [ Html.Attributes.style "font-size" "10rem"
                        , Html.Attributes.style "font-weight" "bold"
                        , Html.Attributes.style "color" "white"
                        , Html.Attributes.style "align-self" "center"
                        , Html.Attributes.style "animation-name" "countdown"
                        , Html.Attributes.style "animation-duration" "1s"
                        , Html.Attributes.style "animation-delay" "0.1s"
                        , Html.Attributes.style "animation-iteration-count" "infinite"
                        ]
                    |> List.singleton
                    |> overlay

        Lost ->
            [ Html.h2
                [ Html.Attributes.style "font-size" "4em"
                , Html.Attributes.style "text-shadow" "2px 2px black"
                ]
                [ Html.text "☠️" ]
            , Html.p
                [ Html.Attributes.style "font-size" "2rem"
                , Html.Attributes.style "text-shadow" "2px 2px black"
                ]
                [ Html.text "Oh, no! Your snake is dead." ]
            , case game.snake.death of
                Nothing ->
                    -- Can't happen?
                    "" |> Html.text

                Just Snake.CollidedWithThemselves ->
                    "The snake have collided with their own body. You need to make sure it doesn't happen!"
                        |> Html.text
                        |> Html.wrap Html.p
                            [ Html.Attributes.style "text-shadow" "2px 2px black"
                            ]

                Just Snake.Burnt ->
                    "Your snake got into a hot flame! Don't play with fire - it's deadly 🔥🔥🔥"
                        |> Html.text
                        |> Html.wrap Html.p
                            [ Html.Attributes.style "text-shadow" "2px 2px black"
                            ]

                Just Snake.CollectedPoo ->
                    "The snake collected a poo and died! That's just disgusting."
                        |> Html.text
                        |> Html.wrap Html.p
                            [ Html.Attributes.style "text-shadow" "2px 2px black"
                            ]
            , playAgainButton
            ]
                |> Html.div
                    [ Html.Attributes.style "font-weight" "bold"
                    , Html.Attributes.style "color" "white"
                    , Html.Attributes.style "text-align" "center"
                    , Html.Attributes.style "align-self" "center"
                    ]
                |> List.singleton
                |> overlay

        Won ->
            [ Html.h2
                [ Html.Attributes.style "font-size" "2em"
                , Html.Attributes.style "text-shadow" "2px 2px black"
                ]
                [ Html.text "👏" ]
            , Html.p
                [ Html.Attributes.style "text-shadow" "2px 2px black"
                ]
                [ Html.text "Bravo! The snake is safe." ]
            , playAgainButton
            ]
                |> Html.div
                    [ Html.Attributes.style "font-size" "2rem"
                    , Html.Attributes.style "font-weight" "bold"
                    , Html.Attributes.style "color" "white"
                    , Html.Attributes.style "text-align" "center"
                    , Html.Attributes.style "align-self" "center"
                    ]
                |> List.singleton
                |> overlay


playAgainButton : Html Msg
playAgainButton =
    Html.button
        [ Html.Events.onClick ContinueButtonClicked
        , Html.Attributes.autofocus True
        , Html.Attributes.style "height" "8rem"
        , Html.Attributes.style "width" "8rem"
        , Html.Attributes.style "font-size" "1.2rem"
        , Html.Attributes.style "font-weight" "bold"
        , Html.Attributes.autofocus True
        ]
        [ Html.text "Play again" ]


viewInstructions : List (Html Never) -> Html Msg
viewInstructions instructions =
    [ instructions |> Html.div [] |> Html.map NoOp
    , "Continue"
        |> Html.text
        |> Html.wrap Html.button
            [ Html.Events.onClick ContinueButtonClicked
            , Html.Attributes.style "align-self" "end"
            , Html.Attributes.style "padding" "1rem 3rem"
            ]
    ]
        |> Html.div
            [ Html.Attributes.style "padding" "1rem"
            , Html.Attributes.style "box-sizing" "border-box"
            , Html.Attributes.style "width" "100%"
            , Html.Attributes.style "max-width" "48rem"
            , Html.Attributes.style "display" "flex"
            , Html.Attributes.style "flex-direction" "column"
            , Html.Attributes.style "gap" "2rem"
            ]


viewSentence : CompleteTheSentence.Model -> Html Msg
viewSentence ({ sentence, collected } as model) =
    let
        background =
            if CompleteTheSentence.isComplete model then
                "lightgreen"

            else
                "lightyellow"

        collectedText =
            if String.isEmpty collected then
                "..."

            else
                collected
    in
    viewHeader
        [ Html.text sentence.intro
        , collectedText
            |> Html.text
            |> Html.wrap Html.span
                [ background |> Html.Attributes.style "background"
                , Html.Attributes.style "padding" "0.2em"
                ]
        , Html.text sentence.outro
        ]


viewHeader : List (Html Msg) -> Html Msg
viewHeader contents =
    Html.div
        [ Html.Attributes.style "text-align" "center"
        , Html.Attributes.style "font-size" "1rem"
        , Html.Attributes.style "font-weight" "bold"
        , Html.Attributes.style "padding" "2rem"
        , Html.Attributes.style "z-index" "1"
        ]
        contents


{-| Displays the snake and it's environment
-}
viewWorld : Model -> Html Msg
viewWorld model =
    let
        world =
            [ viewBackgroundTiles model.tiles
            , viewAnnotations 0 model.annotations
            , viewTemporaryAnnotations model
            , viewEntities model.entities
            , viewSnake model.progress model.snake
            ]
                |> List.concat
                |> Svg.Keyed.node "g"
                    [ model.pan
                        |> Pan.value
                        |> Coordinates.multiply -1
                        |> Coordinates.toTransformation "px"
                        |> Transformations.toString
                        |> Html.Attributes.style "transform"
                    , Html.Attributes.style "pointer-events" "none"
                    ]

        viewbox : String
        viewbox =
            [ model.viewbox // -2
            , model.viewbox // -2
            , model.viewbox
            , model.viewbox
            ]
                |> List.map ((*) scale)
                |> List.map String.fromInt
                |> String.join " "
    in
    Svg.svg
        [ Svg.Attributes.viewBox viewbox
        , Html.Attributes.style "width" "100%"
        , Html.Attributes.style "height" "0"
        , Html.Attributes.style "flex-grow" "1"
        , Html.Attributes.style "background" "#262626" -- From the background image
        , Touch.onStart TouchStarted
        , Touch.onEnd TouchEnded
        , Touch.onMove TouchMoved
        , Touch.onCancel TouchCanceled
        ]
        [ viewDefs
        , world
        ]


{-| These are annotations in the making

For example, while the letters are collected, the annotation shows what has been
collected so far. It will be updated with new letters. Once the whole word is
collected, it will be displayed as a permanent annotation. Permanent annotations
are not to be changed.

-}
viewTemporaryAnnotations : Model -> List ( String, Svg Msg )
viewTemporaryAnnotations model =
    case model.goal of
        CompleteTheSentence completion ->
            if CompleteTheSentence.isComplete completion then
                -- As soon as a sentence is completed it will be added to
                -- annotations (see the step function), so no need to repeat it
                -- here.
                let
                    text =
                        "Right. Now escape the trap!"

                    key =
                        text
                            |> String.latinize
                            |> String.toLower
                            |> String.dasherize
                            |> String.append "temporary-annotation-"

                    fontSize =
                        autoFontSize text

                    coordinates =
                        completion.area.center
                            |> Coordinates.fromPosition scale
                            |> Coordinates.shift (1.5 * scale) South

                    svg =
                        text
                            |> Svg.text
                            |> List.singleton
                            |> viewBigText fontSize coordinates
                in
                [ ( key, svg ) ]

            else
                let
                    fontSize =
                        autoFontSize completion.sentence.password

                    coordinates =
                        completion.area.center
                            |> Coordinates.fromPosition scale

                    visible =
                        []

                    invisible =
                        [ Svg.Attributes.fill "none"
                        , Svg.Attributes.stroke "none"
                        ]

                    key =
                        completion.sentence.password
                            |> String.latinize
                            |> String.toLower
                            |> String.dasherize
                            |> String.append "temporary-annotation-"

                    svg =
                        completion.sentence.password
                            |> String.toList
                            |> List.indexedMap
                                (\index character ->
                                    character
                                        |> String.fromChar
                                        |> Svg.text
                                        |> Html.wrap Svg.tspan
                                            (if index < String.length completion.collected then
                                                visible

                                             else
                                                invisible
                                            )
                                )
                            |> viewBigText fontSize coordinates
                in
                [ ( key, svg ) ]

        _ ->
            []


viewAnnotations : Int -> List Annotation -> List ( String, Svg Msg )
viewAnnotations index annotations =
    case annotations of
        [] ->
            []

        annotation :: rest ->
            ( index |> String.fromInt
            , annotation |> viewAnnotation
            )
                :: viewAnnotations (index + 1) rest


viewAnnotation : Annotation -> Svg Msg
viewAnnotation annotation =
    annotation.text
        |> Svg.text
        |> List.singleton
        |> viewBigText annotation.size annotation.coordinates


viewBigText : Float -> Coordinates -> List (Svg Msg) -> Svg Msg
viewBigText fontSize coordinates text =
    text
        |> Svg.text_
            [ Html.Attributes.style "text-anchor" "middle"
            , Html.Attributes.style "dominant-baseline" "middle"
            , fontSize
                |> (*) scale
                |> String.fromFloat
                |> Svg.Attributes.fontSize
            , Svg.Attributes.fontWeight "bold"
            , Svg.Attributes.fill "hsl(0, 0%, 13%)"
            , Svg.Attributes.stroke "hsl(0, 0%, 16%)"
            , Svg.Attributes.strokeWidth (0.2 * scale |> String.fromFloat)
            , Html.Attributes.style "paint-order" "stroke"
            , Svg.Attributes.strokeLinecap "butt"
            , Svg.Attributes.strokeLinejoin "mitter"
            , coordinates
                |> Coordinates.toTransformation ""
                |> Transformations.toString
                |> Svg.Attributes.transform
            ]


viewBackgroundTiles : Set Position -> List ( String, Svg Msg )
viewBackgroundTiles tiles =
    tiles
        |> Set.toList
        |> List.map viewBackgroundTile


viewBackgroundTile : Position -> ( String, Svg Msg )
viewBackgroundTile ( x, y ) =
    let
        length =
            backgroundTileSize
                |> Position.sectorLength
                |> toFloat

        key =
            [ x, y ]
                |> List.map String.fromInt
                |> String.join "-"
                |> String.append "background-tile-"

        image =
            Svg.image
                [ length * scale |> String.fromFloat |> Svg.Attributes.width
                , length * scale |> String.fromFloat |> Svg.Attributes.height
                , Svg.Attributes.xlinkHref "/cartographer.webp"
                , length * scale / -2 |> String.fromFloat |> Svg.Attributes.x
                , length * scale / -2 |> String.fromFloat |> Svg.Attributes.y
                , Transformations.Translate "px" (toFloat x * scale * length) (toFloat y * scale * length)
                    |> Transformations.toString
                    |> Html.Attributes.style "transform"
                ]
                []
    in
    ( key, image )


viewEntities : Entities -> List ( String, Svg Msg )
viewEntities entities =
    entities
        |> Dict.toList
        |> List.concatMap viewEntitiesAt


viewSnake : Float -> Snake -> List ( String, Svg Msg )
viewSnake progress snake =
    if Snake.isDead snake then
        []

    else
        let
            distance : Float
            distance =
                1 - progress
        in
        -- TODO: Multiple stripes
        -- [ viewStripe 0.8 "#133d1a" points
        -- , viewStripe 0.7 "#106023" points
        -- , viewStripe 0.5 "#1e7d35" points
        -- , viewStripe 0.2 "#5bb171" points
        -- ]
        --     |> Svg.g []
        case snake.body of
            [] ->
                -- Just the head, so no direction.
                '💀'
                    |> String.fromChar
                    |> Svg.text
                    |> Html.wrap Svg.text_
                        [ Html.Attributes.style "text-anchor" "middle"
                        , Html.Attributes.style "dominant-baseline" "middle"
                        , Svg.Attributes.width (scale |> String.fromFloat)
                        , Svg.Attributes.height (scale |> String.fromFloat)
                        , Svg.Attributes.fontSize (0.6 * scale |> String.fromFloat)
                        , Svg.Attributes.fontWeight "bold"
                        , Svg.Attributes.fill "hsl(0, 0%, 86%)"
                        , snake.head
                            |> Coordinates.fromPosition scale
                            |> Coordinates.toTransformation ""
                            |> Transformations.toString
                            |> Svg.Attributes.transform
                        ]
                    |> Tuple.pair "snake-dead"
                    |> List.singleton

            [ neck ] ->
                -- Snake only has one segment. Head goes straight from it.
                let
                    d =
                        [ snake.head
                            |> Coordinates.fromPosition scale
                            |> Coordinates.shift (distance * scale) neck
                            |> Coordinates.toString
                            |> String.append "M "
                        , snake.head
                            |> Coordinates.fromPosition scale
                            |> Coordinates.shift scale neck
                            |> Coordinates.toString
                            |> String.append "L "
                        ]
                            |> String.join "\n"
                            |> Svg.Attributes.d
                in
                Svg.path
                    [ d
                    , 0.3
                        * scale
                        |> String.fromFloat
                        |> Svg.Attributes.strokeWidth
                    , Svg.Attributes.fill "none"
                    , Svg.Attributes.color "green"
                    , Svg.Attributes.stroke "currentColor"
                    , Svg.Attributes.strokeLinecap "round"
                    , Svg.Attributes.strokeLinejoin "round"
                    , Svg.Attributes.markerStart "url(#head-marker)"
                    , Svg.Attributes.markerEnd "url(#tail-marker)"
                    ]
                    []
                    |> Tuple.pair "the-snake"
                    |> List.singleton

            neck :: torso :: tail ->
                -- A proper snake.
                let
                    d : String
                    d =
                        dHead distance neck snake.head
                            :: dNeck
                            :: dTail tipDistance torsoPosition tail
                            |> String.join "\n"

                    dNeck =
                        neckCoordinates
                            |> Coordinates.toString
                            |> String.append "L "

                    neckCoordinates =
                        snake.head
                            |> Position.shift neck
                            |> Coordinates.fromPosition scale
                            |> Coordinates.shift (scale * distance) torso

                    torsoPosition =
                        snake.head
                            |> Position.shift neck
                            |> Position.shift torso

                    tipDistance =
                        if Snake.isGrowing snake then
                            1

                        else
                            distance
                in
                Svg.path
                    [ d |> Svg.Attributes.d
                    , 0.3
                        * scale
                        |> String.fromFloat
                        |> Svg.Attributes.strokeWidth
                    , Svg.Attributes.fill "none"
                    , Svg.Attributes.color "green"
                    , Svg.Attributes.stroke "currentColor"
                    , Svg.Attributes.strokeLinecap "round"
                    , Svg.Attributes.strokeLinejoin "round"
                    , Svg.Attributes.markerStart "url(#head-marker)"
                    , Svg.Attributes.markerEnd "url(#tail-marker)"
                    ]
                    []
                    |> Tuple.pair "the-snake"
                    |> List.singleton


dTail : Float -> Position -> List Direction -> List String
dTail distance start directions =
    case directions of
        [] ->
            [ start
                |> Coordinates.fromPosition scale
                |> Coordinates.toString
                |> String.append "L "
            ]

        tipDirection :: [] ->
            [ start
                |> Coordinates.fromPosition scale
                |> Coordinates.shift (scale * distance) tipDirection
                |> Coordinates.toString
                |> String.append "L "
            ]

        previousDirection :: tipDirection :: [] ->
            -- Tip of the tail needs a spacial treatment. It needs to start on start position  and be extended in a
            let
                next =
                    start
                        |> Position.shift previousDirection
            in
            [ start
                |> Coordinates.fromPosition scale
                |> Coordinates.toString
                |> String.append "L "
            , start
                |> Coordinates.fromPosition scale
                |> Coordinates.shift (scale * distance) previousDirection
                |> Coordinates.toString
                |> String.append "L "
            ]
                ++ dTail distance next [ tipDirection ]

        direction :: rest ->
            let
                dLine =
                    start
                        |> Coordinates.fromPosition scale
                        |> Coordinates.toString
                        |> String.append "L "

                next =
                    start
                        |> Position.shift direction
            in
            dLine :: dTail distance next rest


dHead : Float -> Direction -> Position -> String
dHead distance neck head =
    head
        |> Coordinates.fromPosition scale
        |> Coordinates.shift (distance * scale) neck
        |> Coordinates.toString
        |> String.append "M "


viewDefs : Svg Msg
viewDefs =
    -- TODO: Find a way to reference SVG line markers from external document.
    -- This was drawn in Inkscape (see art/snake.svg) and hand-converted to Elm. Not a very maintainable workflow.
    Svg.defs
        []
        [ Svg.marker
            [ Svg.Attributes.id "tail-marker"
            , Svg.Attributes.markerWidth "2.5"
            , Svg.Attributes.markerHeight "1"
            , Svg.Attributes.refX "0.5"
            , Svg.Attributes.refY "0.5"
            , Svg.Attributes.orient "auto"
            , Svg.Attributes.viewBox "0 0 2.5 1"
            , Svg.Attributes.preserveAspectRatio "xMidYMid"
            ]
            [ Svg.path
                [ Svg.Attributes.fill "green" -- TODO: context-stroke, currently not supported by Firefox?
                , Svg.Attributes.d "M 2.5,0.5 C 2.5,0.25 1.25,0 0.5,0 0.25,0 0,0.25 0,0.5 0,0.75 0.25,1 0.5,1 c 0.75,0 2,-0.25 2,-0.5 z"
                ]
                []
            ]
        , Svg.marker
            [ Svg.Attributes.id "head-marker"
            , Svg.Attributes.markerWidth "1.7499681"
            , Svg.Attributes.markerHeight "1.2613386"
            , Svg.Attributes.refX "1"
            , Svg.Attributes.refY "0.63099998"
            , Svg.Attributes.orient "auto"
            , Svg.Attributes.viewBox "0 0 1.7499681 1.2613386"
            , Svg.Attributes.preserveAspectRatio "xMidYMid"
            ]
            [ Svg.g
                [ Svg.Attributes.transform "translate(3.9999681,-0.37951825)"
                ]
                [ Svg.path
                    [ Svg.Attributes.id "head"
                    , Svg.Attributes.fill "green" -- TODO: context-stroke, currently not supported by Firefox?
                    , Svg.Attributes.d "M -3.0056977,0.39122066 C -2.7499084,0.33493002 -2.75,0.49999998 -2.5,0.49999999 c 0.1540952,2e-8 0.25,0.36855429 0.25,0.50000001 0,0.1753797 -0.1048943,0.5 -0.25,0.5 -0.25,0 -0.2338606,0.1928022 -0.4942705,0.127269 C -3.5,1.5 -3.9999386,1.5 -3.9999681,1.0184896 -4,0.49999999 -3.5,0.50000001 -3.0056977,0.39122066 Z"
                    ]
                    []
                , Svg.path
                    [ Svg.Attributes.id "right-eye"
                    , Svg.Attributes.fill "black"
                    , Svg.Attributes.d "m -3.4247252,0.70346431 a 0.08485731,0.18134895 74.470359 0 0 0.1974477,0.0332056 0.08485731,0.18134895 74.470359 0 0 0.1520089,-0.13031314 0.08485731,0.18134895 74.470359 0 0 -0.1974478,-0.0332055 0.08485731,0.18134895 74.470359 0 0 -0.1520088,0.13031304 z"
                    ]
                    []
                , Svg.path
                    [ Svg.Attributes.id "left-eye"
                    , Svg.Attributes.fill "black"
                    , Svg.Attributes.d "m -3.4247288,1.2965332 a 0.18134895,0.08485731 15.52964 0 1 0.1974476,-0.033205 0.18134895,0.08485731 15.52964 0 1 0.1520089,0.1303128 0.18134895,0.08485731 15.52964 0 1 -0.1974475,0.033205 0.18134895,0.08485731 15.52964 0 1 -0.152009,-0.1303128 z"
                    ]
                    []
                ]
            ]
        ]


viewEntitiesAt : ( Position, Entities.State ) -> List ( String, Svg Msg )
viewEntitiesAt ( ( x, y ) as position, state ) =
    let
        key : Entity -> String
        key entity =
            [ entity |> String.fromChar
            , x |> String.fromInt
            , y |> String.fromInt
            ]
                |> String.join "-"

        withKey : Entity -> Svg Msg -> ( String, Svg Msg )
        withKey entity svg =
            ( key entity
            , svg
            )
    in
    case state of
        Entities.Empty ->
            []

        Entities.Appearing entity ->
            [ entity
                |> viewEntity
                    [ [ position |> Coordinates.fromPosition scale |> Coordinates.toTransformation "px"
                      , Transformations.Scale 0.01 0.01
                      ]
                        |> List.map Transformations.toString
                        |> String.join " "
                        |> Html.Attributes.style "transform"
                    ]
                |> withKey entity
            ]

        Entities.Present entity ->
            [ entity
                |> viewEntity
                    [ [ position |> Coordinates.fromPosition scale |> Coordinates.toTransformation "px"
                      , Transformations.Scale 1 1
                      ]
                        |> List.map Transformations.toString
                        |> String.join " "
                        |> Html.Attributes.style "transform"
                    ]
                |> withKey entity
            ]

        Entities.Disappearing entity ->
            [ entity
                |> viewEntity
                    [ [ position |> Coordinates.fromPosition scale |> Coordinates.toTransformation "px"
                      , Transformations.Scale 0.01 0.01
                      ]
                        |> List.map Transformations.toString
                        |> String.join " "
                        |> Html.Attributes.style "transform"
                    , Html.Attributes.style "opacity" "0.3"
                    ]
                |> withKey entity
            ]

        Entities.Replacing old new ->
            [ old
                |> viewEntity
                    [ [ position |> Coordinates.fromPosition scale |> Coordinates.toTransformation "px"
                      , Transformations.Scale 0.01 0.01
                      ]
                        |> List.map Transformations.toString
                        |> String.join " "
                        |> Html.Attributes.style "transform"
                    , Html.Attributes.style "opacity" "0.3"
                    ]
                |> withKey old
            , new
                |> viewEntity
                    [ [ position |> Coordinates.fromPosition scale |> Coordinates.toTransformation "px"
                      , Transformations.Scale 0.01 0.01
                      ]
                        |> List.map Transformations.toString
                        |> String.join " "
                        |> Html.Attributes.style "transform"
                    ]
                |> withKey new
            ]

        Entities.Collected entity ->
            [ entity
                |> viewEntity
                    [ [ position |> Coordinates.fromPosition scale |> Coordinates.toTransformation "px"
                      , Transformations.Scale 20 20
                      ]
                        |> List.map Transformations.toString
                        |> String.join " "
                        |> Html.Attributes.style "transform"
                    , Html.Attributes.style "opacity" "0.3"
                    ]
                |> withKey entity
            ]


viewEntity : List (Svg.Attribute Msg) -> Entity -> Svg Msg
viewEntity customAttributes entity =
    let
        defaultAttributes =
            [ Html.Attributes.style "text-anchor" "middle"
            , Html.Attributes.style "dominant-baseline" "middle"
            , Svg.Attributes.width (scale |> String.fromFloat)
            , Svg.Attributes.height (scale |> String.fromFloat)
            , Svg.Attributes.fontSize (0.6 * scale |> String.fromFloat)
            , Svg.Attributes.fontWeight "bold"
            , Svg.Attributes.fill "hsl(0, 0%, 86%)"

            -- With the transition enabled, we trigger the Android Chrome bug
            -- https://bugs.chromium.org/p/chromium/issues/detail?id=1412901
            -- , Html.Attributes.style "transition" "transform 200ms, opacity 200ms"
            ]

        attributes =
            defaultAttributes ++ customAttributes
    in
    entity
        |> String.fromChar
        |> Svg.text
        |> Html.wrap Svg.text_ attributes



-- UPDATE


type Msg
    = NoOp Never
    | CountdownClockTick Time.Posix
    | NextFrame Float
    | KeyPressed (Maybe Keyboard.Key)
    | TouchStarted Touch.Event
    | TouchMoved Touch.Event
    | TouchEnded Touch.Event
    | TouchCanceled Touch.Event
    | ContinueButtonClicked


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp _ ->
            -- Handle the Never from Instructions
            ( model, Cmd.none )

        CountdownClockTick _ ->
            ( { model | countdown = model.countdown - 1 }
            , Cmd.none
            )

        NextFrame duration ->
            let
                delta =
                    -- If the performance degrades below 12 FPS, slow down
                    min 83 duration

                pan =
                    Pan.animate delta model.pan

                progress =
                    model.progress + (delta / pace)

                overflow =
                    progress - (progress |> floor |> toFloat)
            in
            ( { model
                | pan = pan
                , progress = overflow
              }
                |> applyIf (progress >= 1) step
            , Cmd.none
            )

        KeyPressed key ->
            case key of
                Nothing ->
                    ( model, Cmd.none )

                Just Keyboard.ArrowUp ->
                    ( { model | snake = Snake.setDirection North model.snake }
                    , Cmd.none
                    )

                Just (Keyboard.Character "W") ->
                    ( { model | snake = Snake.setDirection North model.snake }
                    , Cmd.none
                    )

                Just Keyboard.ArrowRight ->
                    ( { model | snake = Snake.setDirection East model.snake }
                    , Cmd.none
                    )

                Just (Keyboard.Character "D") ->
                    ( { model | snake = Snake.setDirection East model.snake }
                    , Cmd.none
                    )

                Just Keyboard.ArrowDown ->
                    ( { model | snake = Snake.setDirection South model.snake }
                    , Cmd.none
                    )

                Just (Keyboard.Character "S") ->
                    ( { model | snake = Snake.setDirection South model.snake }
                    , Cmd.none
                    )

                Just Keyboard.ArrowLeft ->
                    ( { model | snake = Snake.setDirection West model.snake }
                    , Cmd.none
                    )

                Just (Keyboard.Character "A") ->
                    ( { model | snake = Snake.setDirection West model.snake }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        TouchStarted event ->
            case event.touches of
                [] ->
                    -- This should never happen, but whatever!
                    ( { model | swipe = Nothing }
                    , Cmd.none
                    )

                [ touch ] ->
                    ( { model
                        | swipe =
                            Just
                                { identifier = touch.identifier
                                , from = touch.clientPos
                                , to = touch.clientPos
                                }
                      }
                    , Cmd.none
                    )

                _ ->
                    -- We do not support multiple touches. Cancel the swipe.
                    ( { model | swipe = Nothing }
                    , Cmd.none
                    )

        TouchMoved event ->
            case event.touches of
                [] ->
                    -- This should never happen, but whatever!
                    ( { model | swipe = Nothing }
                    , Cmd.none
                    )

                [ touch ] ->
                    case model.swipe of
                        Nothing ->
                            -- This also shouldn't really happen. Surely there was a touch start some time before!
                            ( { model
                                | swipe =
                                    Just
                                        { identifier = touch.identifier
                                        , from = touch.clientPos
                                        , to = touch.clientPos
                                        }
                              }
                            , Cmd.none
                            )

                        Just swipe ->
                            if swipe.identifier == touch.identifier then
                                ( { model
                                    | swipe =
                                        Just
                                            { swipe | to = touch.clientPos }
                                  }
                                , Cmd.none
                                )

                            else
                                -- There is a swipe, but from a different touch? That is weird. I guess we should start a new swipe then.
                                ( { model
                                    | swipe =
                                        Just
                                            { identifier = touch.identifier
                                            , from = touch.clientPos
                                            , to = touch.clientPos
                                            }
                                  }
                                , Cmd.none
                                )

                _ ->
                    -- We do not support multiple touches. Cancel the swipe.
                    ( { model | swipe = Nothing }
                    , Cmd.none
                    )

        TouchEnded event ->
            case event.changedTouches of
                [] ->
                    -- This should never happen, but whatever!
                    ( { model | swipe = Nothing }
                    , Cmd.none
                    )

                [ touch ] ->
                    case model.swipe of
                        Nothing ->
                            -- This also shouldn't really happen. Surely there was a touch start some time before! Not much we can do here.
                            -- TODO: Report an error.
                            ( model
                            , Cmd.none
                            )

                        Just swipe ->
                            if swipe.identifier == touch.identifier then
                                ( { model
                                    | swipe = Nothing
                                    , snake =
                                        { swipe | to = touch.clientPos }
                                            |> swipeDirection
                                            |> Maybe.map (\direction -> Snake.setDirection direction model.snake)
                                            |> Maybe.withDefault model.snake
                                  }
                                , Cmd.none
                                )

                            else
                                -- There is a swipe, but from a different touch? That is weird. I guess we should start a new swipe then.
                                ( { model
                                    | swipe =
                                        Just
                                            { identifier = touch.identifier
                                            , from = touch.clientPos
                                            , to = touch.clientPos
                                            }
                                  }
                                , Cmd.none
                                )

                _ ->
                    -- We do not support multiple touches. Cancel the swipe.
                    ( { model | swipe = Nothing }
                    , Cmd.none
                    )

        TouchCanceled _ ->
            ( { model
                | swipe = Nothing
              }
            , Cmd.none
            )

        ContinueButtonClicked ->
            -- This Msg is handled in the parent program, see comment in Main.elm
            ( model
            , Cmd.none
            )


step : Model -> Model
step model =
    let
        direction : Direction
        direction =
            model.swipe
                |> Maybe.andThen swipeDirection
                |> Maybe.withDefault model.snake.direction

        area =
            case model.goal of
                CompleteTheSentence completion ->
                    completion.area

                _ ->
                    Area.centerAround snake.head Area.default

        ( entities, randomness ) =
            model.entities
                |> Entities.step
                |> randomDecay decayRatio
                |> Random.andThen
                    (randomSpawn
                        area
                        model.goal
                        model.snake
                    )
                |> (\generator -> Random.step generator model.randomness)

        decayRatio =
            if outcome model == InProgress then
                0.01

            else
                0.3

        goal : Goal
        goal =
            model.goal
                |> updateGoal model.snake model.entities

        snake : Snake
        snake =
            model.snake
                |> Snake.setDirection direction
                |> applyIf (Snake.isAlive model.snake) Snake.move
                |> feed

        feed : Snake -> Snake
        feed feeder =
            model.entities
                |> Entities.get model.snake.head
                |> List.foldl Snake.feed feeder

        tiles =
            snake.head
                |> Position.sector backgroundTileSize
                |> Position.neighborhood
                |> List.foldr Set.insert model.tiles

        burn : Bool
        burn =
            case model.goal of
                CompleteTheSentence _ ->
                    not (Goal.isComplete goal) && Snake.isAlive snake

                _ ->
                    False

        panTo : Coordinates
        panTo =
            -- If there is a fixed area (like in complete the sentence goal) and
            -- the snake is inside it, then pan the camera to the midpoint
            -- between the snake's head and the center of the area. Otherwise
            -- just keep tracking the snake.
            case goal of
                CompleteTheSentence completion ->
                    if Area.isInside area snake.head then
                        snake.head
                            |> Coordinates.fromPosition scale
                            |> Coordinates.midpoint (Coordinates.fromPosition scale completion.area.center)

                    else
                        snake.head
                            |> Coordinates.fromPosition scale

                _ ->
                    snake.head
                        |> Coordinates.fromPosition scale

        pan =
            Pan.setTarget panTo model.pan
    in
    { model
        | goal = goal
        , tiles = tiles
        , pan = pan
        , entities =
            entities
                |> updateEntities model.goal model.snake
                |> applyIf burn (burnTheEdge area snake)
        , randomness = randomness
        , snake = snake
        , swipe = model.swipe |> Maybe.map (\swipe -> { swipe | from = swipe.to })
    }
        |> insertAnnotations model


{-| Annotations are inserted based on changes in the model
-}
insertAnnotations : Model -> Model -> Model
insertAnnotations old new =
    case ( old.goal, new.goal ) of
        ( CompleteTheSentence previous, CompleteTheSentence current ) ->
            let
                annotation =
                    current.area.center
                        |> Coordinates.fromPosition scale
                        |> Annotation current.sentence.password fontSize

                fontSize =
                    autoFontSize current.sentence.password

                justCompleted : Bool
                justCompleted =
                    CompleteTheSentence.isComplete current && not (CompleteTheSentence.isComplete previous)
            in
            applyIf
                justCompleted
                (insertAnnotation annotation)
                new

        ( _, ChangeDirections directions ) ->
            let
                annotation =
                    new.snake.head
                        |> Coordinates.fromPosition scale
                        |> Annotation text fontSize

                text =
                    directions
                        |> List.head
                        |> Maybe.map Direction.toString
                        |> Maybe.map (String.append "Go ")
                        |> Maybe.withDefault "Good!"

                fontSize =
                    2
            in
            applyIf
                (old.goal /= new.goal)
                (insertAnnotation annotation)
                new

        ( _, CollectSingle _ collectibles ) ->
            let
                annotation =
                    new.snake.head
                        |> Coordinates.fromPosition scale
                        |> Annotation text fontSize

                text =
                    collectibles
                        |> List.head
                        |> Maybe.map .label
                        |> Maybe.map (String.append "Find ")
                        |> Maybe.withDefault "Yummy!"

                fontSize =
                    autoFontSize text
            in
            applyIf
                (old.goal /= new.goal)
                (insertAnnotation annotation)
                new

        _ ->
            new


insertAnnotation : Annotation -> Model -> Model
insertAnnotation annotation model =
    { model | annotations = annotation :: model.annotations }


{-| Find a font size that will make given text fit within the area
-}
autoFontSize : String -> Float
autoFontSize text =
    text
        |> String.length
        |> toFloat
        |> (/) 20


updateGoal : Snake -> Entities -> Goal -> Goal
updateGoal snake entities goal =
    case goal of
        CompleteTheSentence completion ->
            entities
                |> Entities.get snake.head
                |> List.foldr (\entity -> CompleteTheSentence.collect entity) completion
                |> CompleteTheSentence

        ReadInstructions _ ->
            goal

        ChangeDirections (required :: following) ->
            if required == snake.direction then
                ChangeDirections following

            else
                goal

        ChangeDirections [] ->
            -- The goal is complete.
            goal

        CollectSingle rubbish collectibles ->
            entities
                |> Entities.get snake.head
                |> List.foldr Goal.collectSingle collectibles
                |> CollectSingle rubbish



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        decodeKeydown : Decode.Value -> Msg
        decodeKeydown event =
            event
                |> Decode.decodeValue Keyboard.eventKeyDecoder
                |> Result.toMaybe
                |> Maybe.andThen Keyboard.anyKeyUpper
                |> KeyPressed

        clock =
            case model.goal of
                ReadInstructions _ ->
                    Sub.none

                _ ->
                    if model.countdown > 0 then
                        Time.every 1000 CountdownClockTick

                    else
                        Browser.Events.onAnimationFrameDelta NextFrame
    in
    [ clock
    , keydown decodeKeydown
    ]
        |> Sub.batch



-- UTILS


{-| Make sure the edge is burning

This function is somewhat (too?) complicated. Basically this is what it does:

  - When the snake approaches the edge it sets it on fire in front of the snake.
  - Sets on fire every position on the edge when there is an adjacent flame.

As a result, the edge starts burning in front of the snake and then the fire spreads all around it. If the snake turns, a new fire is started in front of it, so it is impossible to escape. It creates a nice effect.

-}
burnTheEdge : Area -> Snake -> Entities -> Entities
burnTheEdge area snake entities =
    let
        setOnFire : Position -> Entities -> Entities
        setOnFire position =
            if
                entities
                    |> Entities.get position
                    |> List.member '🔥'
            then
                -- Already burning. Leave it as it is.
                identity

            else
                Entities.spawn position '🔥'

        {- Spread the fire from positions that already burn -}
        spread : Position -> Entities -> Entities
        spread position =
            if
                entities
                    |> Entities.neighbors position
                    |> List.member '🔥'
            then
                setOnFire position

            else
                identity

        {- Start new fire in front of the snake -}
        ignite : Entities -> Entities
        ignite =
            let
                isAhead : Position -> Bool
                isAhead position =
                    snake.head
                        |> Position.line snake.direction 6
                        |> List.member position
            in
            case
                area
                    |> Area.edge
                    |> List.find isAhead
            of
                Nothing ->
                    identity

                Just position ->
                    setOnFire position
    in
    area
        |> Area.edge
        |> List.foldl spread entities
        |> ignite


updateEntities : Goal -> Snake -> Entities -> Entities
updateEntities goal snake entities =
    if Snake.isAlive snake then
        entities
            |> Entities.get snake.head
            |> List.foldr (processCollected goal snake) entities

    else
        snake.body
            |> Position.trace snake.head
            |> List.foldl (\position -> Dict.insert position (Entities.Present '🦴')) entities
            |> Dict.insert snake.head (Entities.Present '💀')


processCollected : Goal -> Snake -> Entity -> Entities -> Entities
processCollected goal snake entity entities =
    case entity of
        '🔥' ->
            Entities.collect snake.head '🔥' entities

        '💩' ->
            Entities.collect snake.head '💩' entities

        _ ->
            case goal of
                CompleteTheSentence completion ->
                    if CompleteTheSentence.isCorrect entity completion then
                        Entities.collect snake.head entity entities

                    else
                        entities
                            |> Entities.decay snake.head
                            |> Entities.spawn (Snake.tail snake) '💩'

                ReadInstructions _ ->
                    entities

                ChangeDirections _ ->
                    Dict.empty

                CollectSingle _ (collectible :: rest) ->
                    if entity == collectible.entity then
                        Entities.collect snake.head entity entities

                    else
                        entities
                            |> Entities.decay snake.head
                            |> Entities.spawn (Snake.tail snake) '💩'

                CollectSingle _ [] ->
                    entities



-- game state predicates


type Outcome
    = Won
    | Lost
    | InProgress


outcome : Model -> Outcome
outcome model =
    if Snake.isDead model.snake then
        Lost

    else
        case model.goal of
            CompleteTheSentence { area } ->
                if Area.isOutside area model.snake.head then
                    -- TODO: Check if sentence is collected? And if not, then what?
                    Won

                else
                    InProgress

            ReadInstructions _ ->
                -- This is always a win
                Won

            ChangeDirections [] ->
                Won

            ChangeDirections _ ->
                InProgress

            CollectSingle _ collectibles ->
                if List.isEmpty collectibles then
                    Won

                else
                    InProgress



-- direction helpers


swipeDirection : Swipe -> Maybe Direction
swipeDirection swipe =
    let
        threshold =
            1.0

        ( fromX, fromY ) =
            swipe.from

        ( toX, toY ) =
            swipe.to

        ( relativeX, relativeY ) =
            ( toX - fromX, toY - fromY )
    in
    if Basics.abs relativeX < threshold || Basics.abs relativeY < threshold then
        Nothing

    else
        Just <|
            if Basics.abs relativeX < Basics.abs relativeY then
                -- north or south
                if relativeY > 0 then
                    South

                else
                    North

            else
            -- east or west
            if
                relativeX > 0
            then
                East

            else
                West



-- random generators


randomSpawn :
    Area
    -> Goal
    -> Snake
    -> Entities
    -> Random.Generator Entities
randomSpawn area goal snake entities =
    let
        isUnderTheSnake position =
            snake.body
                |> Position.trace snake.head
                |> List.member position

        isInFrontOfTheSnake : Position -> Bool
        isInFrontOfTheSnake position =
            snake.head
                |> Position.line snake.direction 5
                |> List.member position

        insert : Entities -> Bool -> Position -> Char -> Entities
        insert existing spawn position entity =
            if spawn && not (isUnderTheSnake position) && not (isInFrontOfTheSnake position) then
                Entities.spawn position entity existing

            else
                existing
    in
    case goal of
        CompleteTheSentence model ->
            let
                probability =
                    0.3

                charset =
                    '🔥' :: model.sentence.charset

                spawnIfNotPresent : Entity -> Entities -> Random.Generator Entities
                spawnIfNotPresent entity existing =
                    if Entities.isPresent entity existing then
                        Random.constant existing

                    else
                        area
                            |> Area.randomPosition
                            |> Random.map
                                (\position ->
                                    insert
                                        existing
                                        True
                                        position
                                        entity
                                )
            in
            case CompleteTheSentence.missing model of
                [] ->
                    Random.constant entities

                next :: following ->
                    Random.map3 (insert entities)
                        (randomBoolean probability)
                        (Area.randomPosition area)
                        (randomEntity next charset)
                        |> Random.andThen (spawnIfNotPresent next)

        ReadInstructions _ ->
            Random.constant Dict.empty

        ChangeDirections _ ->
            Random.constant Dict.empty

        CollectSingle rubbish (collectible :: rest) ->
            let
                probability =
                    0.3

                spawnCollectible : Position -> Entities
                spawnCollectible position =
                    insert entities True position collectible.entity
            in
            if Entities.isPresent collectible.entity entities then
                case rubbish of
                    [] ->
                        Random.constant entities

                    one :: more ->
                        Random.map3 (insert entities)
                            (randomBoolean probability)
                            (Area.randomPosition { area | center = snake.head })
                            (Random.uniform one more)

            else
                -- Always spawn around the snake. Doesn't matter where the original area was.
                { area | center = snake.head }
                    |> Area.randomPosition
                    |> Random.map spawnCollectible

        CollectSingle _ [] ->
            Random.constant Dict.empty


hasWon : Model -> Bool
hasWon game =
    outcome game == Won



-- RANDOM GENERATORS
-- TODO: Consider moving to Entities module.


randomDecay : Float -> Entities -> Random.Generator Entities
randomDecay probability entities =
    let
        decayMany : List Bool -> Entities
        decayMany decayList =
            entities
                |> Dict.keys
                |> List.map2 Tuple.pair decayList
                |> List.foldr maybeDecay entities

        maybeDecay : ( Bool, Position ) -> Entities -> Entities
        maybeDecay ( isDecaying, position ) =
            if isDecaying then
                Entities.decay position

            else
                identity
    in
    probability
        |> randomBoolean
        |> Random.list (Dict.size entities)
        |> Random.map decayMany


randomEntity : Entity -> Goal.Charset -> Random.Generator Entity
randomEntity promoted charset =
    charset
        |> List.map (Tuple.pair 1)
        |> Random.weighted ( 0, promoted )


randomBoolean : Float -> Random.Generator Bool
randomBoolean probability =
    Random.weighted
        ( probability, True )
        [ ( 1 - probability, False ) ]
