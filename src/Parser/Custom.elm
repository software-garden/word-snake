module Parser.Custom exposing
    ( charset
    , intro
    , letter
    , levels
    , line
    , lines
    , outro
    , password
    )

import Goal exposing (Charset, Goal)
import Goal.CompleteTheSentence as CompleteTheSentence
import Parser exposing ((|.), (|=), Parser)


levels : Parser (List Goal)
levels =
    Parser.succeed Basics.identity
        |= charset
        |. Parser.symbol "\n"
        |> Parser.andThen lines


charset : Parser Charset
charset =
    let
        charsetHelper : Charset -> Parser (Parser.Step Charset Charset)
        charsetHelper set =
            Parser.oneOf
                [ letter
                    |> Parser.map (\character -> Parser.Loop (character :: set))
                , Parser.symbol "\n"
                    |> Parser.map (\_ -> Parser.Done (List.reverse set))
                , Parser.spaces
                    |> Parser.map (\_ -> Parser.Loop set)
                ]
    in
    Parser.loop [] charsetHelper


lines : Charset -> Parser (List Goal)
lines set =
    let
        lineHelper : List Goal -> Parser (Parser.Step (List Goal) (List Goal))
        lineHelper memo =
            Parser.oneOf
                [ line set
                    |> Parser.map (\level -> Parser.Loop (level :: memo))
                , Parser.end
                    |> Parser.map (\_ -> Parser.Done (List.reverse memo))
                ]
    in
    Parser.loop [] lineHelper


letter : Parser Char
letter =
    Parser.chompIf
        (\character ->
            not <| List.member character [ '\n', ' ' ]
        )
        |> Parser.getChompedString
        |> Parser.andThen
            (\string ->
                case String.toList string of
                    [] ->
                        Parser.problem "Expacting a letter"

                    [ character ] ->
                        Parser.succeed character

                    _ ->
                        Parser.problem "Expacting a single letter"
            )


line : Charset -> Parser Goal
line set =
    Parser.succeed (CompleteTheSentence.Sentence set)
        |= intro
        |. Parser.symbol "{"
        |. Parser.spaces
        |= password set
        |. Parser.spaces
        |. Parser.symbol "}"
        |= outro
        -- For now area is centered at ( 0, 0 ), but will be reset when the game starts
        |> Parser.map (CompleteTheSentence.init ( 0, 0 ))
        |> Parser.map Goal.CompleteTheSentence


intro : Parser String
intro =
    Parser.chompWhile (\character -> not <| List.member character [ '{', '\n' ])
        |> Parser.getChompedString


outro : Parser String
outro =
    Parser.chompUntil "\n"
        |. Parser.chompIf ((==) '\n')
        |> Parser.getChompedString


password : Charset -> Parser String
password set =
    Parser.chompWhile (\character -> List.member character set)
        |> Parser.getChompedString
