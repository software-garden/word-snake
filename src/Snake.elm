module Snake exposing
    ( CauseOfDeath(..)
    , Snake
    , feed
    , grow
    , isAlive
    , isColliding
    , isDead
    , kill
    , move
    , new
    , setDirection
    , tail, isGrowing
    )

import Direction exposing (Direction)
import Entities exposing (Entity)
import List exposing (length)
import List.Extra as List
import Position exposing (Position)


type alias Snake =
    { head : Position
    , direction : Direction
    , body : List Direction
    , length : Int
    , death : Maybe CauseOfDeath
    }


type CauseOfDeath
    = CollidedWithThemselves
    | Burnt
    | CollectedPoo


new : Position -> Direction -> Int -> Snake
new head direction length =
    { head = head
    , direction = direction
    , body =
        direction
            |> Direction.opposite
            |> List.repeat length
    , length = length
    , death = Nothing
    }


isAlive : Snake -> Bool
isAlive snake =
    snake.death == Nothing


isDead : Snake -> Bool
isDead =
    not << isAlive


move : Snake -> Snake
move snake =
    let
        head : Position
        head =
            Position.shift snake.direction snake.head

        body : List Direction
        body =
            Direction.opposite snake.direction
                :: snake.body
                |> List.take snake.length

        death : Maybe CauseOfDeath
        death =
            if isColliding snake then
                Just CollidedWithThemselves

            else
                snake.death
    in
        { head = head
        , direction = snake.direction
        , body = body
        , length = snake.length
        , death = death
        }



{-| Check if the snake is colliding with itself

    import Direction exposing (Direction(..))

    snake : Snake
    snake =
        { head = ( 0, 3 )
        , direction = South
        , body =
            [ North
            , East
            , South
            , West -- collision here
            , West
            ]
        , length = 5
        , death = Nothing
        }

    isColliding snake
    --> True

-}
isColliding : Snake -> Bool
isColliding snake =
    snake.body
        |> Position.trace ( 0, 0 )
        |> List.drop 1
        |> List.member ( 0, 0 )


setDirection : Direction -> Snake -> Snake
setDirection direction snake =
    case List.head snake.body of
        Nothing ->
            { snake | direction = direction }

        Just neck ->
            if direction == neck then
                snake

            else
                { snake | direction = direction }


feed : Entity -> Snake -> Snake
feed entity snake =
    case entity of
        '🔥' ->
            kill Burnt snake

        '💩' ->
            kill CollectedPoo snake

        _ ->
            grow snake


grow : Snake -> Snake
grow snake =
    { snake | length = snake.length + 1 }


kill : CauseOfDeath -> Snake -> Snake
kill cause snake =
    if isAlive snake then
        { snake | death = Just cause }

    else
        -- You can't kill a dead snake!
        snake


{-| Given a snake, returns the position of it's tail

    import Direction exposing (Direction(..))

    snake : Snake
    snake =
        { head = ( 4, -3 )
        , direction = North
        , body =
            [ East
            , East
            , North
            , West
            , North
            ]
        , length = 6 -- growing
        , death = Nothing
        }

    tail snake
    --> (5, -5)

-}
tail : Snake -> Position
tail snake =
    List.foldl Position.shift snake.head snake.body

isGrowing : Snake -> Bool
isGrowing snake =
    snake.length > List.length snake.body
