{- The Tad.Http Elm module

   Copyright (C) 2021 Tad Lispy

   This library is free software: you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
   details.

   You should have received a copy of the GNU General Public License along with
   this library.  If not, see <https://www.gnu.org/licenses/>.
-}


module Tad.Http exposing (..)

import Http


explainError : Http.Error -> String
explainError error =
    case error of
        Http.BadUrl url ->
            [ "This URL is invalid: "
            , "<"
            , url
            , ">"
            ]
                |> String.join ""

        Http.Timeout ->
            "Request timed out"

        Http.NetworkError ->
            "There was a network error"

        Http.BadStatus status ->
            [ "The response status is not good: "
            , status |> String.fromInt
            ]
                |> String.join ""

        Http.BadBody explanation ->
            explanation
