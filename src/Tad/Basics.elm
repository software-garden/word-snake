module Tad.Basics exposing (applyIf)

{-| Apply a transformation conditionally

Useful in pipelines

-}
applyIf : Bool -> (a -> a) -> a -> a
applyIf yes function argument =
    if yes then
        function argument

    else
        argument
