{- The Tad.Parser Elm module

   Copyright (C) 2021 Tad Lispy

   This library is free software: you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
   details.

   You should have received a copy of the GNU General Public License along with
   this library.  If not, see <https://www.gnu.org/licenses/>.
-}


module Tad.Parser exposing (deadEndsToString, fromResult)

{-| Implements missing deadEndsToString function

Courtesy of Ben Burdette (bburdette) who provided the implementation here:

    <https://github.com/elm/parser/pull/16/files>

Once this PR is merged and released, this file can be dropped.

-}

import Parser exposing (DeadEnd, Parser, Problem(..))


deadEndsToString : List DeadEnd -> String
deadEndsToString deadEnds =
    String.concat
        (List.intersperse "\n" (List.map deadEndToString deadEnds))


deadEndToString : DeadEnd -> String
deadEndToString deadend =
    problemToString deadend.problem ++ " at row " ++ String.fromInt deadend.row ++ ", col " ++ String.fromInt deadend.col


problemToString : Problem -> String
problemToString p =
    case p of
        Expecting s ->
            "expecting '" ++ s ++ "'"

        ExpectingInt ->
            "expecting int"

        ExpectingHex ->
            "expecting hex"

        ExpectingOctal ->
            "expecting octal"

        ExpectingBinary ->
            "expecting binary"

        ExpectingFloat ->
            "expecting float"

        ExpectingNumber ->
            "expecting number"

        ExpectingVariable ->
            "expecting variable"

        ExpectingSymbol symbol ->
            "expecting symbol '" ++ symbol ++ "'"

        ExpectingKeyword keyword ->
            "expecting keyword '" ++ keyword ++ "'"

        ExpectingEnd ->
            "expecting end"

        UnexpectedChar ->
            "unexpected char"

        Problem description ->
            description

        BadRepeat ->
            "bad repeat"


fromResult : Result String a -> Parser a
fromResult result =
    case result of
        Err problem ->
            Parser.problem problem

        Ok value ->
            Parser.succeed value
