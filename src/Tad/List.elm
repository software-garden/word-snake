{- The Tad.List Elm module

   Copyright (C) 2021 Tad Lispy

   This library is free software: you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
   details.

   You should have received a copy of the GNU General Public License along with
   this library.  If not, see <https://www.gnu.org/licenses/>.
-}


module Tad.List exposing (stableSort, stableSortBy)

import List.Extra as List


stableSort : List comparable -> List comparable
stableSort items =
    List.stableSortWith
        (\itemA itemB ->
            compare
                itemA
                itemB
        )
        items


stableSortBy : (a -> comparable) -> List a -> List a
stableSortBy toComparable items =
    List.stableSortWith
        (\itemA itemB ->
            compare
                (toComparable itemA)
                (toComparable itemB)
        )
        items
