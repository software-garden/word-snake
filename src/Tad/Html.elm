{- The Tad.Html Elm module

   Copyright (C) 2021 Tad Lispy

   This library is free software: you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
   details.

   You should have received a copy of the GNU General Public License along with
   this library.  If not, see <https://www.gnu.org/licenses/>.
-}


module Tad.Html exposing (textInput, wrap)

import Html exposing (Html)
import Html.Attributes
import Html.Events


wrap :
    (List (Html.Attribute msg) -> List (Html msg) -> Html msg)
    -> List (Html.Attribute msg)
    -> Html msg
    -> Html msg
wrap wrapper attributes element =
    element
        |> List.singleton
        |> wrapper attributes


textInput : String -> (String -> msg) -> String -> Html msg
textInput label tag value =
    Html.label []
        [ label
            |> Html.text
        , Html.input
            [ Html.Attributes.type_ "text"
            , Html.Attributes.placeholder label
            , Html.Attributes.value value
            , Html.Events.onInput tag
            ]
            []
        ]
