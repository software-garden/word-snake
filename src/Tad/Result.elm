module Tad.Result exposing (..)

import Result.Extra as Result


dropErrors : List (Result e a) -> List a
dropErrors results =
    results
        |> Result.partition
        |> Tuple.first
