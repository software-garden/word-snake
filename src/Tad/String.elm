{- The Tad.String Elm module

   Copyright (C) 2021 Tad Lispy

   This library is free software: you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
   details.

   You should have received a copy of the GNU General Public License along with
   this library.  If not, see <https://www.gnu.org/licenses/>.
-}


module Tad.String exposing (latinize)

import Regex exposing (Regex)


{-| Temporary (?) replacement for String.Extra.removeAccents

Until <https://github.com/elm-community/string-extra/pull/48> is merged.

-}
latinize : String -> String
latinize string =
    if String.isEmpty string then
        string

    else
        let
            do_regex_to_remove_acents ( regex, replace_character ) =
                Regex.replace regex (\_ -> replace_character)
        in
        List.foldl do_regex_to_remove_acents string accentRegex


{-| Create list with regex and char to replace.
-}
accentRegex : List ( Regex.Regex, String )
accentRegex =
    let
        matches =
            [ ( "[à-æ]", "a" )
            , ( "[À-Æ]", "A" )
            , ( "ç", "c" )
            , ( "Ç", "C" )
            , ( "[è-ë]", "e" )
            , ( "[È-Ë]", "E" )
            , ( "[ì-ï]", "i" )
            , ( "[Ì-Ï]", "I" )
            , ( "ñ", "n" )
            , ( "Ñ", "N" )
            , ( "[ò-ö]", "o" )
            , ( "[Ò-Ö]", "O" )
            , ( "[ù-ü]", "u" )
            , ( "[Ù-Ü]", "U" )
            , ( "ý", "y" )
            , ( "ÿ", "y" )
            , ( "Ý", "Y" )

            -- Latin Extended-A
            , ( "Ā", "A" )
            , ( "ā", "a" )
            , ( "Ă", "A" )
            , ( "ă", "a" )
            , ( "Ą", "A" )
            , ( "ą", "a" )
            , ( "Ć", "C" )
            , ( "ć", "c" )
            , ( "Ĉ", "C" )
            , ( "ĉ", "c" )
            , ( "Ċ", "C" )
            , ( "ċ", "c" )
            , ( "Č", "C" )
            , ( "č", "c" )
            , ( "Ď", "D" )
            , ( "ď", "d" )
            , ( "Đ", "D" )
            , ( "đ", "d" )
            , ( "Ē", "e" )
            , ( "ē", "e" )
            , ( "Ĕ", "E" )
            , ( "ĕ", "e" )
            , ( "Ė", "E" )
            , ( "ė", "e" )
            , ( "Ę", "E" )
            , ( "ę", "e" )
            , ( "Ě", "E" )
            , ( "ě", "e" )
            , ( "Ĝ", "G" )
            , ( "ĝ", "g" )
            , ( "Ğ", "G" )
            , ( "ğ", "g" )
            , ( "Ġ", "G" )
            , ( "ġ", "g" )
            , ( "Ģ", "G" )
            , ( "ģ", "g" )
            , ( "Ĥ", "H" )
            , ( "ĥ", "h" )
            , ( "Ħ", "H" )
            , ( "ħ", "h" )
            , ( "Ĩ", "I" )
            , ( "ĩ", "i" )
            , ( "Ī", "I" )
            , ( "ī", "i" )
            , ( "Ĭ", "I" )
            , ( "ĭ", "i" )
            , ( "Į", "I" )
            , ( "į", "i" )
            , ( "İ", "I" )
            , ( "ı", "i" )
            , ( "Ĳ", "IJ" )
            , ( "ĳ", "ij" )
            , ( "Ĵ", "J" )
            , ( "ĵ", "j" )
            , ( "Ķ", "K" )
            , ( "ķ", "k" )
            , ( "ĸ", "K" )
            , ( "Ĺ", "L" )
            , ( "ĺ", "l" )
            , ( "Ļ", "L" )
            , ( "ļ", "l" )
            , ( "Ľ", "L" )
            , ( "ľ", "l" )
            , ( "Ŀ", "L" )
            , ( "ŀ", "l" )
            , ( "Ł", "L" )
            , ( "ł", "l" )
            , ( "Ń", "N" )
            , ( "ń", "n" )
            , ( "Ņ", "N" )
            , ( "ņ", "n" )
            , ( "Ň", "N" )
            , ( "ň", "n" )
            , ( "ŉ", "n" )
            , ( "Ŋ", "N" )
            , ( "ŋ", "n" )
            , ( "Ō", "O" )
            , ( "ō", "o" )
            , ( "Ŏ", "O" )
            , ( "ŏ", "o" )
            , ( "Ő", "O" )
            , ( "ő", "o" )
            , ( "Œ", "OE" )
            , ( "œ", "oe" )
            , ( "Ŕ", "R" )
            , ( "ŕ", "r" )
            , ( "Ŗ", "R" )
            , ( "ŗ", "r" )
            , ( "Ř", "R" )
            , ( "ř", "r" )
            , ( "Ś", "S" )
            , ( "ś", "s" )
            , ( "Ŝ", "S" )
            , ( "ŝ", "s" )
            , ( "Ş", "S" )
            , ( "ş", "s" )
            , ( "Š", "S" )
            , ( "š", "s" )
            , ( "Ţ", "T" )
            , ( "ţ", "t" )
            , ( "Ť", "T" )
            , ( "ť", "t" )
            , ( "Ŧ", "T" )
            , ( "ŧ", "t" )
            , ( "Ũ", "U" )
            , ( "ũ", "u" )
            , ( "Ū", "U" )
            , ( "ū", "u" )
            , ( "Ŭ", "U" )
            , ( "ŭ", "u" )
            , ( "Ů", "U" )
            , ( "ů", "u" )
            , ( "Ű", "U" )
            , ( "ű", "u" )
            , ( "Ų", "U" )
            , ( "ų", "u" )
            , ( "Ŵ", "W" )
            , ( "ŵ", "w" )
            , ( "Ŷ", "Y" )
            , ( "ŷ", "y" )
            , ( "Ÿ", "Y" )
            , ( "Ź", "Z" )
            , ( "ź", "z" )
            , ( "Ż", "Z" )
            , ( "ż", "z" )
            , ( "Ž", "Z" )
            , ( "ž", "z" )
            , ( "ſ", "s" )
            ]

        regexFromString : String -> Regex
        regexFromString =
            Regex.fromString >> Maybe.withDefault Regex.never
    in
    List.map (Tuple.mapFirst regexFromString) matches
