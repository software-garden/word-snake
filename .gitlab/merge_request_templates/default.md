Before merging please make sure that:

- [ ] It works as expected in Firefox on Linux, Mac or Windows (one is enough)
- [ ] It works as expected in Chromium on Linux, Mac or Windows (one is enough)
- [ ] It works as expected in Safari on Linux, Mac or Windows (one is enough)
- [ ] It works as expected in Chrome on Android
- [ ] It works as expected in Firefox on Android
- [ ] It works as expected in Safari on iOS
