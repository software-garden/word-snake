entrypoints = src/index.html src/icon.svg
public-url ?= /

include Makefile.d/defaults.mk

all: ## Run unit tests and build the program (DEFAULT)
all: test dist
.PHONY: all

help: ## Print this help message
help: # TODO: Handle section headers in awk script
	@echo "Useage: make [ goals ]"
	@echo
	@echo "Available goals:"
	@echo
	@cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help

dist: ## Build the program to the dist/ directory
dist: node_modules $(shell find src/ -type f )
	./node_modules/.bin/parcel build \
		--no-autoinstall \
		--public-url=$(public-url) \
		$(entrypoints)
	touch $@

test: ## Test the program
test:
	elm-verify-examples
	elm-test
.PHONY: test

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	mkdir -p $(prefix)
	cp --recursive dist/* $(prefix)
.PHONY: install

node_modules: package.json package-lock.json
	rm -rf $@
	npm clean-install

package-lock.json: package.json
	npm install --package-lock-only
	touch $@


### DEVELOPMENT

develop: ## Start a development server with hot reloading
develop: node_modules
	npx parcel \
		--no-autoinstall \
		--public-url=$(public-url) \
		$(entrypoints)
.PHONY: develop

serve: ## Serve the built program
serve: dist
	miniserve --index=index.html dist
.PHONY: serve

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX
.PHONY: clean

clean-soft: ## Just clean the caches as they often jam the build tools
clean-soft: artifacts := dist/
clean-soft: artifacts += .cache/
clean-soft: artifacts += elm-stuff/
clean-soft:
	rm -rf $(artifacts)
.PHONY: clean-soft

clean-hard: ## Like clean, but also remove the generated files that are version controlled
clean-hard: clean clean-nix


### ANDROID APP

android: ## Build android program in the android/ directory
android: clean-soft
android: dist
android: resources/splash.png
android: resources/icon.png
android: resources/android/icon-foreground.png
android: resources/android/icon-background.png
android:
	$(error Android builds are broken for now)
	npx cordova-res android --skip-config --copy
	npx cap sync android
.PHONY: android

resources/splash.png: src/splash.jpg
	mkdir -p resources/
	vips copy $< $@

resources/icon.png: src/icon.svg
	mkdir -p resources/
	inkscape \
		--export-width=2048 \
		--export-height=2048 \
		--export-filename=$@ \
		$<

resources/android/icon-foreground.png: src/icon.svg
	mkdir -p resources/android/
	inkscape \
		--export-width=2048 \
		--export-height=2048 \
		--export-area-page \
		--export-id-only \
		--export-id=foreground \
		--export-filename=$@ \
		$<

resources/android/icon-background.png: src/icon.svg
	mkdir -p resources/android/
	inkscape \
		--export-width=2048 \
		--export-height=2048 \
		--export-area-page \
		--export-id-only \
		--export-id=background \
		--export-filename=$@ \
		$<


### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result: node-dependencies.nix node-env.nix node-packages.nix elm-packages.nix
result:
	# $(error Pure Nix build is broken due to Elm NPM package that always tries to download stuff from the Internet)
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)

serve-result: ## Serve the program built using Nix
serve-result: result
	miniserve --index=index.html result
.PHONY: serve-result

node-dependencies.nix node-env.nix node-packages.nix: package-lock.json
	rm -rf node_modules
	node2nix --lock package-lock.json --composition node-dependencies.nix --development

elm-packages.nix: elm.json elm-registry.dat
	elm2nix convert > $@

elm-registry.dat:
# Workaround for https://github.com/cachix/elm2nix/issues/43
	elm2nix snapshot > registry.dat
	mv registry.dat $@

clean-nix: ## Remove generated nix expressions
clean-nix: artifacts := node-dependencies.nix
clean-nix: artifacts += node-env.nix
clean-nix: artifacts += node-packages.nix
clean-nix: artifacts += elm-packages.nix
clean-nix: artifacts += elm-registry.dat
clean-nix:
	rm -rf $(artifacts)
.PHONY: clean-nix
