{
  description = "Word Snake game";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-compat, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        node-dependencies = (
          pkgs.callPackage ./node-dependencies.nix { inherit pkgs system; }
        ).nodeDependencies.override {
          buildInputs = [
            pkgs.pkg-config
            pkgs.vips
            pkgs.python
            pkgs.nodePackages.node-gyp-build
            pkgs.elmPackages.elm
          ];
        };
        fetchElmDeps = pkgs.elmPackages.fetchElmDeps {
          elmPackages = import ./elm-packages.nix;
          registryDat = ./elm-registry.dat;
          elmVersion = (builtins.fromJSON (
            builtins.readFile ./elm.json
          )).elm-version;
        };
        shared-inputs = [
          pkgs.android-tools
          pkgs.gradle
          pkgs.inkscape
          pkgs.vips
          pkgs.gnumake
          pkgs.nodejs
          pkgs.elmPackages.elm
          pkgs.utillinux
          pkgs.elmPackages.elm-test
          pkgs.elmPackages.elm-verify-examples
        ];
      in rec {
        packages.word-snake = pkgs.stdenv.mkDerivation {
          name = "word-snake";
          src = self;
          buildInputs = shared-inputs ++ [
            node-dependencies
          ];

          patchPhase = ''
            ln -s ${node-dependencies}/lib/node_modules ./node_modules
            ${fetchElmDeps}
          '';
        };

        devShell = pkgs.mkShell {
          name = "word-snake-dev-shell";
          src = self;
          buildInputs = shared-inputs ++ [
            pkgs.nodePackages.node2nix
            pkgs.elm2nix
            pkgs.cachix
            pkgs.jq
            pkgs.miniserve
          ];
        };

        defaultPackage = packages.word-snake;
      }
    );
}
